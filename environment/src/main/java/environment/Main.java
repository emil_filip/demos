package environment;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Random;

public class Main extends Application {
    private static Group root = new Group();

    private static Scene scene;

    private Random r = new Random();

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        stage.setTitle("Environment");
        int screenWidth = 1000;
        int screenHeight = 700;

        Main.scene = new Scene(
                Main.root,
                screenWidth,
                screenHeight,
                Color.rgb(40, 40, 40)
        );

        stage.setResizable(false);
        stage.setOnCloseRequest(
                event -> {
                    //do stuff
                }
        );
        stage.setOnShown(
                event -> {
                    //do stuff
                }
        );

        stage.setScene(Main.scene);
        stage.show();
    }
}
