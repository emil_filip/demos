package environment.Entities;

interface Moveable {
    void start();
    void stop();

    void move();
}
