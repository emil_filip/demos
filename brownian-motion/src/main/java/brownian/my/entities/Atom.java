package brownian.my.entities;

import brownian.Main;
import brownian.my.utils.MotionCalculator;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Atom extends Rectangle implements Moveable {

    private final long id;

    private final AtomicInteger speed = new AtomicInteger();
    private final AtomicBoolean stopped = new AtomicBoolean(true);

    private static final Paint DEFAULT_FILL = Color.rgb(0, 0, 255);
    private static final Paint STARTED_FILL = Color.rgb(0, 255, 0);
    private static final Paint STOPPED_FILL = Color.rgb(255, 0, 0);

    private long lastMoved = 0L;

    public Atom(long id) {
        this(id, Default.WIDTH.value, 20, 0, 0, 2);
    }

    public Atom(long id, double width, double height) {
        this(id, width, height, 0, 0, 2);
    }

    public Atom(long id, double width, double height, double x, double y, int speed) {
        super(x, y, width, height);
        this.id = id;
        this.setSpeed(speed);
        this.setFill(DEFAULT_FILL);
    }

    @Override
    public boolean start() {
        if (this.stopped.compareAndSet(true, false)){
            this.setFill(STARTED_FILL);
            return true;
        };
        return false;
    }

    @Override
    public boolean stop() {
        if (this.stopped.compareAndSet(false, true)) {
            this.setFill(STOPPED_FILL);
            return true;
        }
        return false;
    }

    @Override
    public boolean move() {
        return this.move(this.calcNextMovement());
    }

    public boolean move(double[] movement) {
        return this.move(movement[0], movement[1]);
    }

    public synchronized boolean move(double x, double y) {
        long nowCalled = System.currentTimeMillis();
        if (!isStopped()) {
            if (this.lastMoved != 0L) {
                long interval = nowCalled - this.lastMoved;
                double framesPassed = interval / (1000 / Main.TARGET_FPS);
                x *= framesPassed;
                y *= framesPassed;
            }
            final double dX = x;
            final double dY = y;
            Platform.runLater(() -> this.setX(this.getX() + dX));
            Platform.runLater(() -> this.setY(this.getY() + dY));
            this.lastMoved = nowCalled;
            return true;
        }
        this.lastMoved = nowCalled;
        return false;
    }

    public double[] calcNextMovement() {
        return MotionCalculator.randomDirFixedSpeed(this.speed.get());
    }

    // -------------------------------------------------------------------------------------------------------------- //

    public long getID() {
        return this.id;
    }

    public synchronized void setToDefaults() {
        this.setWidth(Default.WIDTH.getValue());
        this.setHeight(Default.HEIGHT.getValue());
        this.setSpeed(Default.SPEED.getValue());
        this.stopped.set(true);
        this.setFill(DEFAULT_FILL);
    }

    public void setSpeed(int newSpeed) {
        this.speed.set(newSpeed);
    }

    public int getSpeed() {
        return this.speed.get();
    }

    public boolean isStopped() {
        return stopped.get();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    public enum Default {
        WIDTH(20),
        HEIGHT(20),
        SPEED((2*60) / Main.TARGET_FPS);

        private int value;

        Default(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}