package brownian.my.entities;

interface Moveable {
    boolean start();
    boolean stop();

    boolean move();
}
