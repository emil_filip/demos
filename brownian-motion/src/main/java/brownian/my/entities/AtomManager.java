package brownian.my.entities;

import brownian.Main;
import brownian.my.utils.Randomized;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class AtomManager implements AutoCloseable {

    private State state;

    private LinkedList<Atom> atomsManaging = new LinkedList<>();
    private ExecutorService atomThreadPool;

    private final Timeline moveTimer = new Timeline(Main.TARGET_FPS);
    private final int limit;

    private int moveCounter = 0;

    private AtomManager(int limit) {
        this.moveTimer.setCycleCount(Animation.INDEFINITE);
        this.moveTimer.getKeyFrames().add(new KeyFrame(Duration.millis(1000/Main.TARGET_FPS), this::handleEvent));
        this.state = State.WAITING;
        this.limit = limit;
    }

    private void handleEvent(ActionEvent actionEvent) {
        int atomNum = this.atomsManaging.size();
        int atomChunks = (int) atomNum / limit;
        int atomsPerChunk = this.limit;
        if (atomChunks != 0) {
            atomsPerChunk = Math.round(atomNum / atomChunks);
        }
        int atomsSkipped = 0;
        if (atomNum > this.limit) {
            atomsSkipped = (moveCounter % atomChunks) * atomsPerChunk;
        }
        atomsManaging.stream()
                     .skip(atomsSkipped)
                     .limit(atomsPerChunk)
                     .forEach(Atom::move);
        ++this.moveCounter;
    }

    public static AtomManager newFixed(int threads, int limit) {
        AtomManager am = new AtomManager(limit);
        am.atomThreadPool = threads > 1 ? Executors.newFixedThreadPool(threads)
                                        : Executors.newSingleThreadExecutor();
        return am;
    }

    public static AtomManager newCached(int limit) {
        AtomManager am = new AtomManager(limit);
        am.atomThreadPool = Executors.newCachedThreadPool();
        return am;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    @Override
    public void close() throws InterruptedException {
        this.atomThreadPool.shutdown();
        if (!this.atomThreadPool.awaitTermination(10, TimeUnit.SECONDS)) {
            this.atomThreadPool.shutdownNow();
            this.atomThreadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        }
    }

    public void startManaging() {
        if (this.getState() == State.WAITING) {
            this.startAllAtoms();
            this.moveTimer.play();
            this.state = State.RUNNING;
        }
    }

    public void stopManaging() throws InterruptedException {
        this.stopAllAtoms();
        this.moveTimer.stop();
        this.close();
        this.removeAllAtoms();
        this.state = State.STOPPED;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    public void addAtom(Atom atom) {
        this.atomsManaging.add(atom);
    }

    public void addAtom(Supplier<Atom> atomSupplier) {
        Atom newAtom = atomSupplier.get();
        this.addAtom(newAtom);
    }

    public void addAtoms(int numAtoms, Supplier<Atom> atomSupplier) {
        for (int i = 0; i < numAtoms; i++) {
            this.addAtom(atomSupplier);
        }
    }

    public void addAtoms(Stream<Atom> atoms) {
        atoms.forEach(this::addAtom);
    }

    public void addAtoms(Collection<Atom> atoms) {
        atoms.forEach(this::addAtom);
    }

    public synchronized void forEachAtom(Consumer<? super Atom> consumer) {
        this.atomsManaging.forEach(consumer);
    }

    public void removeAtom(Atom atom) {
        if (!this.atomsManaging.remove(atom)){
            throw new IllegalArgumentException("atom given is not found");
        }
    }

    public void removeAtom(Atom atom, Consumer<? super Atom> consumer) {
        consumer.accept(atom);
        removeAtom(atom);
    }

    public synchronized void removeAllAtoms() {
        this.atomsManaging = new LinkedList<>();
    }

    public void startAllAtoms() {
        this.forEachAtom((Atom atom) -> {
            if (atom.isStopped()) {
                atom.start();
            }
        });
    }

    public void stopAllAtoms() {
        this.forEachAtom((Atom atom) -> {
            if (!atom.isStopped()) {
                atom.stop();
            }
        });
    }

    // -------------------------------------------------------------------------------------------------------------- //

    public int getTotalAtoms() {
        return this.atomsManaging.size();
    }

    public int getIndexOfAtom(Atom atom) {
        return this.atomsManaging.indexOf(atom);
    }

    public LinkedList<Atom> getAllAtoms() {
        return atomsManaging;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    public enum State {
        WAITING,
        RUNNING,
        STOPPED,
    }

    public State getState() {
        return state;
    }
}
