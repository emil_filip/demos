package brownian.my.entities;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;

public class AtomFactory {

    private static long idCounter = 0L;
    private static final AtomicReference<LinkedList<Atom>> atomCache = new AtomicReference<>(new LinkedList<>());

    private AtomFactory(){}

    public static Atom getAtom() {
        return retrieve();
    }

    public static Atom getDefaultAtom() {
        Atom a = retrieve();
        a.setToDefaults();
        return a;
    }

    public static Atom getSpecificAtom(double width, double height) {
        Atom a = getDefaultAtom();
        a.setWidth(width);
        a.setHeight(height);
        return a;
    }

    public static Atom getSpecificAtom(double width, double height, double x, double y) {
        Atom a = getSpecificAtom(width, height);
        a.setX(x);
        a.setY(y);
        return a;
    }

    public static Atom getSpecificAtom(double width, double height, double x, double y, int speed) {
        Atom a = getSpecificAtom(width, height, x, y);
        a.setSpeed(speed);
        return a;
    }

    public static void returnAtom(Atom atom) {
        atomCache.get().addLast(atom);
    }

    // -------------------------------------------------------------------------------------------------------------- //

    private static synchronized Atom retrieve() {
        if (atomCache.get().size() == 0) {
            long i = idCounter;
            ++idCounter;
            return new Atom(i);
        }
        Atom a = atomCache.get().get(0);
        atomCache.get().remove(a);
        return a;
    }
}
