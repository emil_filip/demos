package brownian.my.utils;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Comparator;
import java.util.Random;
import java.util.stream.Stream;

public final class Randomized {

    private Randomized() {
        //
        // No instances
        //
    }

    public static <T> Stream<T> of(Stream<T> source, Random rnd) {
        return source.map(x -> Pair.of(x, rnd.nextDouble()))       // associate value with random number
                     .sorted(Comparator.comparing(Pair::getRight)) // sort by associated random number
                     .map(Pair::getLeft);                          // chop the random number off
    }
}
