package brownian.my.utils;

import java.util.concurrent.ThreadLocalRandom;

public class MotionCalculator {

    public static double[] randomDirFixedSpeed(int speed) {
        final ThreadLocalRandom r = ThreadLocalRandom.current();
        final double x = r.nextDouble(-speed, speed);
        final double y = Math.pow(Math.pow(speed, 2) - Math.pow(x, 2), 0.5) * (r.nextBoolean() ? 1 : -1);
        return new double[]{x, y};
    }
}
