package brownian;

import brownian.my.entities.AtomFactory;
import brownian.my.entities.AtomManager;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;

public class Main extends Application {
    private static Group root = new Group();
    private static Scene scene;

    public static final int CORES = Runtime.getRuntime().availableProcessors();
    public static final int TARGET_FPS = 60;

    private Random r = new Random();

    public static final int NUMBER_OF_ATOMS = 1200;
    public static final int MOVE_LIMIT = 100 * CORES;
    public static final int SCREEN_WIDTH = 1000;
    public static final int SCREEN_HEIGHT = 700;

    // Frame rate logger vars.
    private static final long[] frameTimes = new long[100];
    private static int frameTimeIndex = 0 ;
    private static boolean arrayFilled = false ;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        stage.setTitle("Brownian Motion");

        Main.scene = new Scene(
                Main.root,
                SCREEN_WIDTH,
                SCREEN_HEIGHT,
                Color.rgb(0, 0, 0)
        );

        AtomManager am = AtomManager.newCached(MOVE_LIMIT);
        am.addAtoms(NUMBER_OF_ATOMS, AtomFactory::getDefaultAtom);
        am.forEachAtom(atom -> {
            root.getChildren().add(atom);
            atom.setX(SCREEN_WIDTH * r.nextDouble());
            atom.setY(SCREEN_HEIGHT * r.nextDouble());
            atom.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    am.removeAtom(atom);
                    root.getChildren().remove(atom);
                    AtomFactory.returnAtom(atom);
                } else if (event.getButton() == MouseButton.PRIMARY) {
                    if (atom.isStopped()) {
                        atom.start();
                    } else {
                        atom.stop();
                    }
                }
            });
        });

        scene.addEventHandler(KeyEvent.KEY_PRESSED,
                              event -> {
                                  if (event.getCode() == KeyCode.SPACE) {
                                      if (am.getState() == AtomManager.State.WAITING) {
                                          am.startManaging();
                                      } else if (am.getState() == AtomManager.State.RUNNING) {
                                          am.startAllAtoms();
                                      }
                                  }
                              });

        final Text coreNum = new Text(2.0, 18.0, String.format("Cores: %d", CORES));
        coreNum.setFont(Font.font("OCR A Extended", 20.0));
        coreNum.setFill(Color.rgb(255, 0, 255));
        root.getChildren().add(coreNum);

        final Text atomNum = new Text(2.0, 40.0, "Atoms: 0");
        atomNum.setFont(Font.font("OCR A Extended", 20.0));
        atomNum.setFill(Color.rgb(255, 0, 255));
        root.getChildren().add(atomNum);

        final Text frameRateText = new Text(2.0, 62.0, "FPS: 0");
        frameRateText.setFont(Font.font("OCR A Extended", 20.0));
        frameRateText.setFill(Color.rgb(255, 0, 255));
        root.getChildren().add(frameRateText);

        stage.setResizable(false);
        stage.setOnCloseRequest(event -> {
                                    try {
                                        am.stopManaging();
                                    } catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                });
        stage.setOnShown(event -> new AnimationTimer() {
                                      public void handle(long now) {
                                          atomNum.setText(String.format("Atoms: %d", am.getTotalAtoms()));
                                          //
                                          // Calculate the current frame rate
                                          //
                                          long oldFrameTime = frameTimes[frameTimeIndex] ;
                                          frameTimes[frameTimeIndex] = now ;
                                          frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length ;
                                          if (frameTimeIndex == 0) {
                                              arrayFilled = true ;
                                          }
                                          if (arrayFilled) {
                                              long elapsedNanos = now - oldFrameTime ;
                                              long elapsedNanosPerFrame = elapsedNanos / frameTimes.length ;
                                              double frameRate = 1000000000.0 / elapsedNanosPerFrame ;
                                              frameRateText.setText(String.format("FPS: %.3f", frameRate));
                                          }
                                      }}.start());

        stage.setScene(Main.scene);
        stage.show();
    }
}
