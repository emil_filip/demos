package ConcurrentLinkedList;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

public class LinkedList<T> {

    private final AtomicReference<Node<T>> head = new AtomicReference<>();

    /**
     * Gets whether the list is empty.
     * @return true if list is empty.
     */
    public boolean isEmpty() {
        return this.head.get() == null;
    }

    /**
     * Inserts a value at the front of the list.
     * @param value value to insert.
     */
    public void pushFront(T value) {
        Objects.requireNonNull(value, "value cannot be null");
        final Node<T> newHead = new Node<>(value);
        while(true) {
            Node<T> oldHead = this.head.get();
            newHead.setNext(oldHead);
            if (this.head.compareAndSet(oldHead, newHead)) {
                break;
            }
        }
    }

    /**
     * Gets the value at the front of the list, if any.
     * @return optional value at the front.
     */
    public Optional<T> peekFront() {
        return Optional.ofNullable(this.head.get()).map(Node::getValue);
    }

    /**
     * Removes the value at the front of the list and returns it, if any.
     * @return optional value removed.
     */
    public Optional<T> popFront() {
        while (true) {
            Node<T> oldHead = head.get();
            Node<T> newHead = oldHead != null ? oldHead.getNext() : null;
            if (head.compareAndSet(oldHead, newHead)) {
                Optional<T> result = Optional.ofNullable(oldHead).map(Node::getValue);
                oldHead.setNext(null);
                return result;
            }
        }
    }

    /**
     * Gets the size of the list.
     * @return list size.
     */
    public int size() {
        Node<T> ptr = head.get();
        int size = 0;
        while(ptr != null) {
            ++size;
            ptr = ptr.getNext();
        }
        return size;
    }

    /**
     * Calls a method on each value in the list.
     * @param proc method called.
     */
    public void forEach(Consumer<T> proc) {
        Node<T> ptr = head.get();
        while(ptr != null) {
            proc.accept(ptr.getValue());
            ptr = ptr.getNext();
        }
    }

    /**
     * Replaces the values in the list, one by one.
     * @param func replacement function.
     */
    public void map(Function<T,T> func) {
        Node<T> ptr = head.get();
        while(ptr != null) {
            synchronized (ptr) {
                ptr.setValue(func.apply(ptr.getValue()));
            }
            ptr = ptr.getNext();
        }
    }
}