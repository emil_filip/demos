package ConcurrentLinkedList;

import java.util.concurrent.atomic.AtomicReference;

class Node<T> {
    private T                        value;
//    private AtomicReference<Node<T>> next;
    private volatile Node<T>         next;

    public Node(T value) {
        this(value, null);
    }

    public Node(T value, Node<T> next) {
        this.value = value;
//        this.next.set(next);
        this.next = next;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getNext() {
//        return this.next.get();
        return this.next;
    }

    void setNext(Node<T> next) {
//        this.next.set(next);
        this.next = next;
    }

//    boolean compareAndSetNext(Node<T> expected, Node<T> replacement) {
//        return this.next.compareAndSet(expected, replacement);
//    }
}
