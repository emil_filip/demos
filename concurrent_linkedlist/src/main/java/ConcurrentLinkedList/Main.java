package ConcurrentLinkedList;

public class Main {

    private static LinkedList<String> list = new LinkedList<>();

    public static void main(String[] args) throws InterruptedException {
//        Thread first = new Thread(Main::test, "first");
//        Thread second = new Thread(Main::test, "second");
//        Thread third = new Thread(Main::test, "third");
//        first.join();
//        second.join();
//        third.join();
//        first.start();
//        second.start();
//        third.start();
//        Thread.sleep(1_500);
//        System.out.println();
//        System.out.println("------ALL THREADS FINISHED------");
//        System.out.printf("------FINAL %d LIST ITEMS:------%n", list.size());
//        list.forEach(System.out::println);
        test();
    }

    private static void test() {
        System.out.println(
                "Is my list empty? ".concat(list.isEmpty() ? "Yes it is." : "No it has some items inside it.")
        );
        System.out.printf("------%s IS ADDING ITEMS TO LIST...------%n", Thread.currentThread().toString());
        list.pushFront("list.");
        list.pushFront("linked");
        list.pushFront("my");
        list.pushFront("is");
        list.pushFront("This");
        System.out.println(
                "Is my list empty? ".concat(list.isEmpty() ? "Yes it is." : "No it has some items inside it.")
        );
        System.out.printf("The size of my list is %d.%n", list.size());
        System.out.printf("------%s IS LOOKING INSIDE...------%n", Thread.currentThread().toString());
        System.out.printf("The first item in my list is '%s'.%n", list.peekFront().get());
        System.out.println("All of the items in my list are:");
        list.forEach(System.out::println);
        System.out.println("Let's remove the first item.");
        String removed = list.popFront().get();
        System.out.printf("We just removed the item '%s'.%n", removed);
        System.out.println("Now let's look at all the items again:");
        list.forEach(System.out::println);
        System.out.println("Awesome! Let's add that first item again...");
        list.pushFront("This");
        System.out.printf("------%s IS MAPPING THE LIST...------%n", Thread.currentThread().toString());
        System.out.println("Now let's map all the strings in the list to their uppercase equivalent.");
        list.map(String::toUpperCase);
        System.out.println("The items in the list are now:");
        list.forEach(System.out::println);
        System.out.println("Now let's reverse every string item in the list...");
        list.map(
                (val) -> new StringBuilder(val).reverse().toString()
        );
        list.forEach(System.out::println);
    }
}
