public class Out<T> {
    public T value;
    public Out() {
        this.value = null;
    }
    public Out(T value) {
        this.value = value;
    }
}
