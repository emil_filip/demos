/**
 * Created by Emilian on 08/07/2018.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("------STARTING PROGRAM------");
        Account account = new Account("Emil");
        AccountPeople people = new AccountPeople(account);
        people.begin();
        Thread.sleep(10_000);
        people.stop();
        System.out.println();
        System.out.printf("The balance on %s's account is £%f", account.getOwner(), account.getBalance());
    }
}
