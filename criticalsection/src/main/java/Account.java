import java.util.function.Function;

public class Account {
    private final String owner;
    private volatile double balance;

    public Account(String owner) {
        this(owner, 0.0);
    }

    public Account(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public double deposit(double amount) {
        if (amount < 0.0) {
            throw new IllegalArgumentException("Cannot deposit amount less than 0.");
        }
        synchronized (this) {
            this.balance += amount;
            return this.balance;
        }
    }

    public synchronized double withdraw(Function<Account, Double> amountProvider, Out<Double> withdrawn) {
        withdrawn.value = amountProvider.apply(this);
        if (withdrawn.value < 0) {
            throw new IllegalArgumentException("amountProvider provided returns amount less than 0");
        }
        return this.withdraw(withdrawn.value);
    }

    // -------------------------------------------------------------------------------------------------------------- //

    private double withdraw(double amount) {
        this.balance -= amount;
        return this.balance;
    }
}
