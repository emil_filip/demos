import java.util.concurrent.ThreadLocalRandom;

public class AccountPeople {

    private volatile Account account;
    private final Thread[] people;

    private boolean enough = true;

    public AccountPeople(Account account) {
        this.account = account;

        this.people = new Thread[3];
        this.people[0] = new Thread(this::displayAccount, "DISPLAY");
        this.people[1] = new Thread(this::depositIntoAccount, "DEPOSIT");
        this.people[2] = new Thread(this::withdrawFromAccount, "WITHDRAW");
    }

    public void begin() {
        if (!enough) {
            throw new IllegalArgumentException("Cannot start twice");
        } else {
            enough = false;
            for (Thread p : this.people) {
                p.start();
            }
        }
    }

    public void stop() {
        if (enough) {
            throw new IllegalArgumentException("Cannot stop twice");
        } else {
            enough = true;
        }
    }

    // -------------------------------------------------------------------------------------------------------------- //

    private void displayAccount() {
        while (!enough) {
            final String accOwner = this.account.getOwner();
            final double accBalance = this.account.getBalance();
            System.out.printf("DISPLAY: The balance on %s's account is £%f. %n", accOwner, accBalance);
            sleepRandom(3_000);
        }
    }

    private void depositIntoAccount() {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        while (!enough) {
            System.out.println("Depositing...");
            final double amount = r.nextDouble(0.0, 1_000.0);
            this.account.deposit(amount);
            System.out.printf("Deposited £%f into %s's account.%n", amount, this.account.getOwner());
            sleepRandom(3_000);
        }
    }

    private void withdrawFromAccount() {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        while (!enough) {
            System.out.println("Withdrawing...");
            if (this.account.getBalance() == 0.0){
                System.out.printf("Cannot withdraw! %s's account has a balance of £0.00%n", account.getOwner());
            } else {
                final Out<Double> withdrawn = new Out<>();
                this.account.withdraw(a -> r.nextDouble(a.getBalance()), withdrawn);
                System.out.printf("Withdrawn £%f from %s's account.%n", withdrawn.value, this.account.getOwner());
            }
            sleepRandom(3_000);
        }
    }

    private void sleepRandom(double bound) {
        final ThreadLocalRandom r = ThreadLocalRandom.current();
        try {
            final double time = r.nextDouble(bound);
            System.err.printf("Thread %s sleeping for %f seconds...%n", Thread.currentThread().getName(), time);
            Thread.sleep((long) time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}