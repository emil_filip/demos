package linkedlist;

public class Cell<T> {
    T value;
    Cell<T> next;
}
