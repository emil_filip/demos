package linkedlist;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class LinkedList<T> {

    Cell<T> head;

    /**
     * Gets whether the list is empty.
     * @return true if list is empty.
     */
    public boolean isEmpty() {
        throw new NotImplementedException();
    }

    /**
     * Inserts a value at the front of the list.
     * @param value value to insert.
     */
    public void pushFront(T value) {
        throw new NotImplementedException();
    }

    /**
     * Gets the value at the front of the list, if any.
     * @return optional value at the front.
     */
    public Optional<T> peekFront() {
        throw new NotImplementedException();
    }

    /**
     * Removes the value at the front of the list and returns it, if any.
     * @return optional value removed.
     */
    public Optional<T> popFront() {
        throw new NotImplementedException();
    }

    /**
     * Gets the size of the list.
     * @return list size.
     */
    public int size() {
        throw new NotImplementedException();
    }

    /**
     * Calls a method on each value in the list.
     * @param proc method called.
     */
    public void forEach(Consumer<T> proc) {
        throw new NotImplementedException();
    }

    /**
     * Replaces the values in the list, one by one.
     * @param fun replacement function.
     */
    public void map(Function<T,T> fun) {
        throw new NotImplementedException();
    }
}